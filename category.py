# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.model import ModelView, ModelSQL, fields


class EventCategory(ModelSQL, ModelView):
    'Event Category'
    __name__ = 'staff.event_category'
    code = fields.Char('Code')
    name = fields.Char('Name', required=True, select=True)
    absenteeism = fields.Boolean("Absenteeism")
    type_event = fields.Selection([
        ('healt_care', 'Healt Care'),
        ('legal', 'Legal'),
        ('', ''), ], 'Type Event')

    @classmethod
    def __setup__(cls):
        super(EventCategory, cls).__setup__()
        cls._order.insert(0, ('name', 'ASC'))


class ListDisease(ModelSQL, ModelView):
    'List Disease'
    __name__ = 'list.disease'
    name = fields.Char('Name', required=True)
    code = fields.Char('Code', required=True)
