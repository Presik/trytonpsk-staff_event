# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from decimal import Decimal
from trytond.pool import PoolMeta, Pool
from trytond.model import ModelView, ModelSQL, Workflow, fields
from trytond.wizard import Wizard, StateView, Button, StateAction
from trytond.transaction import Transaction
from trytond.report import Report
from trytond.pyson import Eval

STATES = {'readonly': (Eval('state') != 'draft')}
_ZERO = Decimal('0')


class Employee(metaclass=PoolMeta):
    __name__ = 'company.employee'
    events = fields.One2Many('staff.event', 'employee', 'Events',
        readonly=True)


class Event(Workflow, ModelSQL, ModelView):
    'Staff Event'
    __name__ = 'staff.event'
    employee = fields.Many2One('company.employee', 'Employee',
                               required=True, states=STATES)
    category = fields.Many2One('staff.event_category', 'Category',
                               required=True, states=STATES)
    event_date = fields.Date('Event Date', required=True, states=STATES)
    start_date = fields.Date('Date Start', required=True, states=STATES)
    end_date = fields.Date('Date End', states={
                'required': True,
                'readonly': Eval('state') == 'done',
        }, depends=['state'])
    reference = fields.Char("Reference", states=STATES)
    description = fields.Char("Description", states=STATES)
    days = fields.Function(fields.Integer("Days"), 'get_days')
    uom = fields.Function(fields.Many2One("product.uom", "UoM", states=STATES),
        'get_uom')
    quantity = fields.Float("Quantity", states=STATES)
    state = fields.Selection([
            ('draft', 'Draft'),
            ('in_progress', 'In Progress'),
            ('done', 'Done'),
        ], 'State', readonly=True)
    absenteeism = fields.Boolean("Absenteeism", states={'readonly': True})
    amount = fields.Numeric('Amount', digits=(16, 2))
    contract = fields.Many2One('staff.contract', 'Contract')
    available_days = fields.Function(fields.Numeric('Available Days',
        states={
            'invisible': ~Eval('is_vacations'),
            }), 'get_days_available')
    days_of_vacations = fields.Numeric('Days Of Vacations', states={
        'invisible': ~Eval('is_vacations'),
        })
    is_vacations = fields.Boolean('Is Vacations')
    party_to_pay = fields.Many2One('party.party', 'Party to Pay',
        states=STATES)
    disease = fields.Many2One('list.disease', 'Disease', states={
        'invisible': Eval('category_type_event') != 'healt_care',
    }, depends=['category_type_event'])
    category_type_event = fields.Function(
        fields.Selection('get_category_type_event', "Category Type Event"),
        'on_change_with_category_type_event')
    lines_payroll = fields.One2Many('staff.payroll.line', 'origin',
        'Lines Payroll', readonly=True)
    total_amount = fields.Function(fields.Numeric('Total Amount',
        digits=(16, 2)), 'get_total_amount')
    unit_price_formula = fields.Char('Unit Price Formula',
            help="""Python expression for eval(expr) \b
            -salary: the salary employee \b
            -amount: field amount """)

    @classmethod
    def __setup__(cls):
        super(Event, cls).__setup__()
        cls._transitions |= set((
            ('draft', 'in_progress'),
            ('in_progress', 'draft'),
            ('in_progress', 'done'),
            ('done', 'draft'),
            ('done', 'paid'),
            ('paid', 'draft'),
            ))
        cls._buttons.update({
            'draft': {
                'invisible': Eval('state') == 'draft',
                },
            'in_progress': {
                'invisible': Eval('state') != 'draft',
                },
            'done': {
                'invisible': Eval('state') != 'in_progress',
                },
        })

    @classmethod
    def __register__(cls, module_name):
        super(Event, cls).__register__(module_name)
        cursor = Transaction().connection.cursor()
        table_h = cls.__table_handler__(module_name)

        if table_h.column_exist('line_payroll'):
            table_h.drop_column('line_payroll')

        if table_h.column_exist('quantity_pay'):
            cursor.execute(
                'UPDATE staff_event set quantity=quantity_pay where quantity_pay is not null and quantity is Null;'
            )
            table_h.drop_column('quantity_pay')
        if table_h.column_exist('cost_amount'):
            cursor.execute(
                'UPDATE staff_event set amount=cost_amount where cost_amount is not null and amount is Null;'
            )
            table_h.drop_column('cost_amount')

        if table_h.column_exist('amount_to_pay'):
            cursor.execute(
                'UPDATE staff_event set amount=amount_to_pay where amount_to_pay is not null and amount is Null;'
            )
            table_h.drop_column('amount_to_pay')

        if table_h.column_exist('quantity_discount'):
            cursor.execute(
                'UPDATE staff_event set quantity=quantity_discount where quantity_discount is not null and quantity is Null;'
            )
            table_h.drop_column('quantity_discount')

    @classmethod
    def get_category_type_event(cls):
        pool = Pool()
        Category = pool.get('staff.event_category')
        return Category.fields_get(['type_event'])['type_event']['selection']

    @fields.depends('category', '_parent_category.type_event')
    def on_change_with_category_type_event(self, name=None):
        if self.category:
            return self.category.type_event

    def get_rec_name(self, name):
        name = ''
        if self.category:
            name = self.category.name

        if self.employee:
            name = name + '[' + self.employee.party.id_number + ']'
        return name

    @classmethod
    def compute_formula(cls, formula, args):
        '''
        Compute a formula field with a salary value as float
        :return: A decimal
        '''
        # formula = getattr(self, 'unit_price_formula')
        if args.get('salary') is not None:
            salary = float(args['salary'])
        amount = 0
        if self.amount:
            amount = float(self.amount)
        value = Decimal(str(round(eval(formula), 2)))
        return value

    def get_total_amount(self, name=None):
        amount = []
        for line in self.lines_payroll:
            amount.append(line.amount)
        return sum(amount)

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_is_vacations():
        return False

    @classmethod
    def process_event(cls, events):
        pool = Pool()
        Contract = pool.get('staff.contract')
        for event in events:
            if event.category and event.is_vacations:
                contract = event.contract
                Contract.write([contract], {'events_vacations': [
                    ('add', [event])]})

    @classmethod
    def force_draft(cls, events):
        pool = Pool()
        Contract = pool.get('staff.contract')
        for event in events:
            if event.category and event.contract and event.id in event.contract.events_vacations:
                contract = event.contract
                Contract.write(
                    [contract], {'events_vacations': [('remove', [event])]}
                )

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, records):
        for record in records:
            cls.force_draft(records)

    @classmethod
    @ModelView.button
    @Workflow.transition('in_progress')
    def in_progress(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    def done(cls, records):
        for record in records:
            cls.process_event(records)

    def get_uom(self, name):
        if self.category and self.category.wage_type:
            return self.category.wage_type.uom.id

    # def get_uom_discount(self, name):
    #     if self.category and self.category.wage_type_discount:
    #         return self.category.wage_type_discount.uom.id

    def get_days(self, name):
        res = None
        if self.end_date:
            res = (self.end_date - self.start_date).days + 1
            if res < 0:
                res = 0
        return res

    @fields.depends('employee', 'contract')
    def on_change_employee(self):
        if not self.employee:
            self.contract = None
        else:
            self.contract = self.employee.contract

    @fields.depends('category', 'is_vacations', 'absenteeism')
    def on_change_category(self):
        if self.category:
            self.absenteeism = self.category.absenteeism
            if self.category.wage_type.type_concept == 'holidays':
                self.is_vacations = True
        else:
            self.absenteeism = False
            self.is_vacations = False

    def get_days_available(self, name):
        res = 0
        if self.is_vacations:
            Contract = Pool().get('staff.contract')
            id = None
            if self.contract and self.contract.id:
                id = self.contract.id
            contracts = Contract.search(
                [('id', '=', id)])
            days = 0
            if contracts:
                contract = contracts[0]
                if contract and contract.events_vacations:
                    for e in contract.events_vacations:
                        days += e.days_of_vacations if e.days_of_vacations else 0
                res = Decimal(
                    round(self.contract.time_worked / 30 * 1.25, 0)) - days
        return res


class EventReport(Report):
    __name__ = 'staff.event'


class EventRegisterStart(ModelView):
    'Staff Event Register Start'
    __name__ = 'staff_event.register.start'
    start_date = fields.Date('Start Date', required=True)
    end_date = fields.Date('End Date', required=True)
    category = fields.Many2One('staff.event_category', 'Category')
    company = fields.Many2One('company.company', 'Company', required=True)
    department = fields.Many2One('company.department', 'Department')

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_end_date():
        Date_ = Pool().get('ir.date')
        return Date_.today()


class EventRegister(Wizard):
    'Event Register'
    __name__ = 'staff_event.register'
    start = StateView(
        'staff_event.register.start',
        'staff_event.event_register_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-ok', default=True),
        ])
    print_ = StateAction('staff_event.report_event_register')

    def do_print_(self, action):
        category_id = None
        department_id = None
        department_name = None
        if self.start.category:
            category_id = self.start.category.id

        if self.start.department:
            department_id = self.start.department.id
            department_name = self.start.department.name

        data = {
            'ids': [],
            'company': self.start.company.id,
            'start_date': self.start.start_date,
            'end_date': self.start.end_date,
            'department': department_id,
            'department_name': department_name,
            'category': category_id,
        }
        return action, data

    def transition_print_(self):
        return 'end'


class EventRegisterReport(Report):
    __name__ = 'staff_event.register.report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)

        pool = Pool()
        Event = pool.get('staff.event')
        Company = pool.get('company.company')
        user = pool.get('res.user')(Transaction().user)

        Event = Pool().get('staff.event')
        dom_events = [
            ('event_date', '>=', data['start_date']),
            ('event_date', '<=', data['end_date']),
        ]
        if data['department']:
            dom_events.append(
                ('employee.department', '=', data['department'])
            )

        if data['category']:
            dom_events.append(
                ('category', '=', data['category'])
            )

        events = Event.search(dom_events)
        total_amount = []
        total_days = []

        for event in events:
            if event.days:
                total_days.append(event.days)
            total_amount.append(event.amount or _ZERO)

        report_context['records'] = events
        report_context['company'] = Company(data['company'])
        report_context['total_days'] = sum(total_days)
        report_context['total_amount'] = sum(total_amount)
        report_context['user'] = user
        report_context['department'] = data['department_name']
        report_context['start_date'] = data['start_date']
        report_context['end_date'] = data['end_date']
        return report_context
